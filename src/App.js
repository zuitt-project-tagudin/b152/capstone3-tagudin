//import React from 'react';
import {useState, useEffect} from 'react';

import {Container} from 'react-bootstrap';

import {BrowserRouter as Router} from 'react-router-dom';
import {Route,Routes} from 'react-router-dom';

import AppNavBar from './components/AppNavBar';


import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import ErrorPage from './pages/ErrorPage';
import Logout from './pages/Logout'
import ViewProduct from './pages/ViewProduct'
import Cart from './pages/Cart'
import Orders from './pages/Orders'
import AllOrders from './pages/AllOrders'

import { UserProvider } from './userContext';

import './App.css';




export default function App() {  


  const [user, setUser] = useState({

      id: null,
      isAdmin: null
  })

  useEffect(() => {

    fetch('https://peaceful-anchorage-75608.herokuapp.com/users/getUserDetails', {

        method: 'GET',
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {

      setUser({

        id: data._id,
        isAdmin: data.isAdmin

      })

    })

  }, [])

  const unsetUser = () => {

      localStorage.clear();

  }


  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>        
        <Router>        
          <AppNavBar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/products" element={<Products />} />
              <Route path="/products/viewProduct/:productId" element={<ViewProduct />} />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/cart" element={<Cart />} />
              <Route path="/orders" element={<Orders />} />
              <Route path="/allOrders" element={<AllOrders />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="*" element={<ErrorPage />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
    )

}