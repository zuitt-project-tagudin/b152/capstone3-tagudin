import {useContext} from 'react';

import {Nav, Navbar, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import UserContext from '../userContext';

export default function AppNavBar() {

	const {user} = useContext(UserContext);



	return (

		<Navbar bg="secondary" expand="lg" sticky="top">
			<Container fluid>
				<Navbar.Brand href="/">The Guild</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="">
						<Link to="/" className="nav-link">Home</Link>
						<Link to="/products" className="nav-link">Products</Link>
						{
						    user.id
						    ? 
						    	user.isAdmin
						    	?
						    	<>
						    		<Link to="/logout" className="nav-link">Logout</Link>
						    	</>
						    	:
						    	<Nav className="" >
						    		<Link to="/cart" className="nav-link">Cart</Link>
						    		<Link to="/orders" className="nav-link">Orders</Link>
						    		<Link  to="/logout" className="nav-link">Logout</Link>
						    	</Nav>
						    :
						    <>
								<Link to="/login" className="nav-link">Login</Link>
								<Link to="/register" className="nav-link">Register</Link>
						    </>
						}
						
					</Nav>
				</Navbar.Collapse>				
			</Container>
		</Navbar>

		)


}