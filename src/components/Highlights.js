import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights() {



	return (

		<Row className="my-2">
			<Col xs={12} md={4}>
				<Card className="p-3 cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Indispensible Healing Items!</h2>
						</Card.Title>
						<Card.Text>
							Low on health? Our shops stock unlimited supplies of healing items for you and your party's HP needs! Pop a Potion when you're low on health, or spread some Lifepowder when your party's in a pinch!
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="p-3 cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Materials for Bombs and Traps!</h2>
						</Card.Title>
						<Card.Text>
							No more traps for your Capture Quests? Trap Tools are in stock! Want your target to be blown to smithereens? Barrels and Bombs are also available for all of your Kaboom desires!
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="p-3 cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Every kind of Primary Ammo!</h2>
						</Card.Title>
						<Card.Text>
							Out of ammo? We stock every kind of primary ammo! From Spread, to Pierce, and the good ol' Normals. We even have Status Ammo such as Sleep, Poison, and Paralysis.  Restock now!
						</Card.Text>
						<Card.Text className="text-end text-muted" style={{fontSize: 10}}>
							 *(elemental ammo and cluster bombs not in stock)
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

		)

}