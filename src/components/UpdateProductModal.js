import {useState} from 'react';
import {Form, Button, Modal} from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function UpdateModal({product}) {

  const[name, setName] = useState("");
  const[description, setDesc] = useState("");
  const[price, setPrice] = useState("");

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

  function editProduct(e) {
    
    e.preventDefault();

    console.log(product);

    console.log(name);
    console.log(description);
    console.log(price);

    let token =localStorage.getItem('token');

    fetch(`https://peaceful-anchorage-75608.herokuapp.com/products/updateProduct/${product._id}`, {

      method: 'PUT',
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token}`
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(data._id) {

        Swal.fire({
          icon: "success",
          title: "Product Update Successful",
        })
      } else {

        Swal.fire({
          icon: "error",
          title: "Failed",
          text: data.message
        })

      }

    })


    handleClose();
    window.location.href = "/products";
  }


	return (

		<>
		<Button variant="warning" className="mx-2" onClick={handleShow}>Edit</Button>
		<Modal show={show} onHide={handleClose} centered>
        <Modal.Header closeButton>
          <Modal.Title>Edit Item: {product.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group>
            <Form.Label>Name:</Form.Label>
            <Form.Control type="text" required value={name} placeholder={product.name} onChange={e => {setName(e.target.value)}} />
          </Form.Group>
          <Form.Group>
            <Form.Label>Description:</Form.Label>
            <Form.Control type="text" required value={description} placeholder={product.description} onChange={e => {setDesc(e.target.value)}} />
          </Form.Group>
          <Form.Group>
            <Form.Label>Price:</Form.Label>
            <Form.Control type="number" required value={price} placeholder={product.price} onChange={e => {setPrice(e.target.value)}} />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={editProduct}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
		</>

		)

}