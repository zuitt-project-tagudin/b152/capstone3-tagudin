
import {Col, Card, OverlayTrigger, Popover} from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function FeaturedBlocks({productProp}) {
	

	//console.log(count);




	const overlayDeets = (

		<Popover className="text-center">
			<Popover.Header style={{fontWeight: "bold"}}>{productProp.name}</Popover.Header>
			<Popover.Body>{productProp.price}z</Popover.Body>
		</Popover>

		);

	return (


			<Col sm={6} md={2} className="my-2" >				
				<OverlayTrigger trigger={["hover", "hover"]} placement="top" overlay={overlayDeets}>

					<Link to={`/products/viewProduct/${productProp._id}`} >
						
						<Card className="p-1 cardHighlight">
							<Card.Body className="text-center">
								<img className="p-1 pop" height={74} src={productProp.url} />	
							</Card.Body>
						</Card>

					</Link>

				</OverlayTrigger>
			</Col>



		)

}