import {useState, useEffect, useContext} from 'react';
import {Table, Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../userContext';
import UpdateModal from './UpdateProductModal';
import CreateModal from '../pages/AddProductModal';


export default function AdminDashboard() {

	const {user} = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);

	function archive(productId) {

		//console.log(productId);
		fetch(`https://peaceful-anchorage-75608.herokuapp.com/products/archive/${productId}`, {

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			window.location.href="/products";
		})
	}

	function activate(productId) {

		//console.log(productId);
		fetch(`https://peaceful-anchorage-75608.herokuapp.com/products/activate/${productId}`, {

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			window.location.href="/products";
		})
	}




	useEffect(() => {

		if (user.isAdmin){

			fetch('https://peaceful-anchorage-75608.herokuapp.com/products/', {
				headers: {
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setAllProducts(data.map(product => {

					return (

						<tr key={product._id}>
							<td className="text-center"><img className="p-1" height={35} src={product.url} /></td>
							<td>{product._id}</td>
							<td>{product.name}</td>
							<td>{product.price}z</td>
							<td>{product.isActive ? "Active" : "Inactive"}</td>
							<td className="text-center">
								<UpdateModal product={product} />
								{
									product.isActive
									? 
									<Button variant="danger" className="mx-2" onClick={() => {archive(product._id)}} >Archive</Button>
									:
									<Button variant="success" className="mx-2" onClick={() => {activate(product._id)}} >Activate</Button>
								}
							</td>
						</tr>

						)

				}))
			})

		}

	}, [])


	return (

		<>

			<h1 className="my-4 text-center" >Admin Dashboard</h1>
			<div className="">
				
			</div>
			<Row className="pb-3 justify-content-center">
				<Col md={2}>
					<Link to="/allOrders">
					<Button variant="warning" >View All Orders</Button>
					</Link>
				</Col>
				<Col md={2} className="text-center">					
					<CreateModal />
				</Col>
			</Row>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th className="text-center">Icon</th>
						<th>ID</th>
						<th>Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
						{allProducts}
				</tbody>
			</Table>
		</>
	)

}