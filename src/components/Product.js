import {useState} from 'react'

import {Col, Card} from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function ProductCard({productProp}) {
	

	//console.log(count);


	return (


			<Col sm={6} md={3} className="my-2" >				
				<Card className="p-1 cardHighlight">
					<Card.Body className="text-center d-flex flex-column">
						<img className="p-1 align-self-center" height={74} src={productProp.url} />
						<Card.Title>
							{productProp.name}
						</Card.Title>
						<Card.Text>
							{productProp.description}
						</Card.Text>
						<div className="mt-auto">
						<Card.Text>
							Price: {productProp.price}z
						</Card.Text>
						<Link to={`/products/viewProduct/${productProp._id}`} className="btn btn-primary" >View Product</Link>
						</div>
					</Card.Body>
				</Card>
			</Col>



		)

}