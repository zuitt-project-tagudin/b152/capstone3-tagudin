import {useState, useEffect} from 'react';
import {Row} from 'react-bootstrap';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import FeaturedBlocks from '../components/Featured'
export default function Home() {

/*	let sampleProp = "I am sample data passed from Home component to Banner component."*/



	let bannerData = {

		title: "Welcome to the Guild Shop!",
		description: "For your everyday Hunting Needs!!!",
		buttonText: "View all of our Products!",
		destination: "/products"

	}

	const [productsArray, setProductsArray] = useState([]);

	useEffect(() => {

		fetch('https://peaceful-anchorage-75608.herokuapp.com/products/getActiveProducts')
		.then(res => res.json())
		.then(data => {

			let featuredArray = [];

			function pullFeatured() {	

				for (let i = 1; i < 6; i++) {
					let rand = Math.floor(Math.random() * data.length)
					featuredArray.push(data.splice(rand,1)[0])
				
				}

			}

			pullFeatured();



			setProductsArray(featuredArray.map(product => {

				return (
					
						<FeaturedBlocks key={product._id} productProp={product} />
					
					)
			}));


		})

	}, [])


	return (

			<>
				<Banner bannerProp={bannerData} />
				<Highlights />
				<h4 className="text-center py-3">Hover over some of our items below:</h4>
				<Row className="py-2 justify-content-center align-self-center">{productsArray}</Row>
			</>

		)

}