import {useState, useEffect, useContext} from 'react';
import {Card, Button, Row, Col, Form} from 'react-bootstrap';
import {useParams, Link, Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import {add} from 'cart-localstorage';
import UserContext from '../userContext';

export default function ViewProduct() {

	const {productId} = useParams();

	const{user} = useContext(UserContext);


	const [product, setProduct] = useState({
		id: null,
		name: null,
		description: null,
		price: null,
		url: null
	})

	const [quantity, setQuantity] = useState(1);

	useEffect(() => {

		fetch(`https://peaceful-anchorage-75608.herokuapp.com/products/getSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProduct({
				id: data._id,
				name: data.name,
				description: data.description,
				price: data.price,
				url: data.url
			})
		})

	}, [productId])



	let total = quantity * product.price

	function addToCart() {

		console.log(total)
		console.log(product)
		add(product, parseInt(quantity))

		Swal.fire({
					icon: "success",
					title: "Successfully added to Cart",
					timer: 2000
				})

		window.setTimeout(() => {window.location.href="/cart"}, 1500);
	}


	return (

		user.isAdmin
		? <Navigate to="/products" replace={true} />
		:
		<Row className="my-5 justify-content-center align-self-center" >
			<Col md={4} >
				<Card bg="light">
					<Card.Body className="text-center" >
						<img className="py-3 " src={product.url} />
						<Card.Title>{product.name}</Card.Title>
						<Card.Text>{product.description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{product.price}z</Card.Text>
						<Form.Group as={Row} className="py-1 justify-content-center" >
							<Form.Label column sm="5">Quantity:</Form.Label>
							<Col sm="4">
							<Form.Control type="number" required defaultValue={1} min={1} onChange={e => {setQuantity(e.target.value)}} />
							</Col>
						</Form.Group>
						<Card.Text className="pt-3">Total: {total}z</Card.Text>
					</Card.Body>
					{
						user.id
						? <Button variant="primary" className="btn-block" onClick={addToCart} >Add to Cart</Button>
						: <Link className="btn btn-danger btn-block" to="/login" > Login to Purchase</Link>
					}
				</Card>
			</Col>
		</Row>

	)

}