import {useState, useEffect, useContext} from 'react';
import {Table} from 'react-bootstrap';
import UserContext from '../userContext';




export default function AllOrders() {

	const {user} = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);




	useEffect(() => {

		if (user.isAdmin){

			fetch('https://peaceful-anchorage-75608.herokuapp.com/orders/getAllOrders', {
				headers: {
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setAllProducts(data.map(order => {

					return (

						<tr key={order._id}>
							<td>{order._id}</td>
							<td>{order.userId}</td>
							<td>{order.totalAmount}z</td>
							<td>{order.purchasedOn}</td>
						</tr>

						)

				}))
			})

		}

	}, [])



	return (

		<>
			<h1 className="my-4 text-center" >All Orders</h1>

			<Table striped bordered hover>
				<thead>
					<tr>
						<th>ID</th>
						<th>User ID</th>
						<th>Total</th>
						<th>Purchased On</th>
					</tr>
				</thead>
				<tbody>
						{allProducts}
				</tbody>
			</Table>
		</>
	)

}