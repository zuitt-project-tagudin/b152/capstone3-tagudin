import {useState} from 'react';
import {Form, Button, Modal} from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function CreateModal(e) {

	const[name, setName] = useState("");
	const[description, setDesc] = useState("");
	const[price, setPrice] = useState("");
	const[url, setUrl] = useState("");


	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	function createProduct(e) {

		e.preventDefault();

		console.log(name);
		console.log(description);
		console.log(price);

		let token =localStorage.getItem('token');

		fetch('https://peaceful-anchorage-75608.herokuapp.com/products/', {

			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				url: url
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data._id) {

				Swal.fire({
					icon: "success",
					title: "Product Added",
				})
				handleClose();
				window.location.href="/products"

			} else {

				Swal.fire({
					icon: "error",
					title: "Failed",
					text: data.message
				})

			}

		})

	}

	return (

		<>
			<Button variant="warning" className="mx-2" onClick={handleShow}>Add Product</Button>
			<Modal show={show} onHide={handleClose} centered>
		        <Modal.Header closeButton>
		          <Modal.Title>Add Item</Modal.Title>
		        </Modal.Header>
		        <Modal.Body>
		          <Form.Group>
		            <Form.Label>Name:</Form.Label>
		            <Form.Control type="text" required value={name} onChange={e => {setName(e.target.value)}} />
		          </Form.Group>
		          <Form.Group>
		            <Form.Label>Description:</Form.Label>
		            <Form.Control type="text" required value={description} onChange={e => {setDesc(e.target.value)}} />
		          </Form.Group>
		          <Form.Group>
		            <Form.Label>Price:</Form.Label>
		            <Form.Control type="number" required value={price} onChange={e => {setPrice(e.target.value)}} />
		          </Form.Group>
		          <Form.Group>
		            <Form.Label>Image URL</Form.Label>
		            <Form.Control type="text" required value={url} onChange={e => {setUrl(e.target.value)}} />
		          </Form.Group>
		        </Modal.Body>
		        <Modal.Footer>
		          <Button variant="secondary" onClick={handleClose}>
		            Close
		          </Button>
		          <Button variant="primary" onClick={createProduct}>
		            Save Changes
		          </Button>
		        </Modal.Footer>
		    </Modal>
		</>

	)

}