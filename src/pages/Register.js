import {useState, useContext, useEffect} from 'react';
import {Form, Button, Row, Col, Card} from 'react-bootstrap';

import UserContext from '../userContext';

import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {

	const {user} = useContext(UserContext);

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPass, setConfirmPass] = useState("");

	const [isActive, setIsActive] = useState(false);

	useEffect(() => {

		if (firstName !== "" &&
			lastName !== "" &&
			email !== "" &&
			mobileNo !== "" &&
			mobileNo.length === 11 &&
			password === confirmPass
			) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [firstName, lastName, email, mobileNo, password, confirmPass])


	function registerUser(e) {

		e.preventDefault();

/*		console.log(firstName);
		console.log(lastName);
		console.log(email);
		console.log(mobileNo);
		console.log(password);
		console.log(confirmPass);*/

		fetch('https://peaceful-anchorage-75608.herokuapp.com/users/', {

			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password

			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
			
			if(data.email) {
				Swal.fire({
					icon:"success",
					title: "Registration Successful",
					text: "Thank you for registering!"
				})

				window.location.href = "/login";

			} else {
				Swal.fire({
					icon: "error",
					title: "Registration failed",
					text: "Something went wrong."
				})
			}

		})
	}


	return (

		user.id
		?
		<Navigate to="/products" replace={true} />
		:
		<>
			<h1 className="my-4 text-center" >Register</h1>
			<Row className="justify-content-center">
				<Col md={6} >
					<Card >
						<Card.Body>
							<Form onSubmit={e => registerUser(e)}>
								<Form.Group>
									<Form.Label>First Name:</Form.Label>
									<Form.Control type="text" placeholder="Enter First Name" required value={firstName} onChange={e => {setFirstName(e.target.value)}} />
								</Form.Group>
								<Form.Group>
									<Form.Label>Last Name:</Form.Label>
									<Form.Control type="text" placeholder="Enter Last Name" required value={lastName} onChange={e => {setLastName(e.target.value)}} />
								</Form.Group>
								<Form.Group>
									<Form.Label>Email:</Form.Label>
									<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}} />
								</Form.Group>
								<Form.Group>
									<Form.Label>Mobile No:</Form.Label>
									<Form.Control type="number" placeholder="Enter Mobile Number" required value={mobileNo} onChange={e => {setMobileNo(e.target.value)}} />
								</Form.Group>
								<Form.Group>
									<Form.Label>Password:</Form.Label>
									<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e => {setPassword(e.target.value)}} />
								</Form.Group>
								<Form.Group>
									<Form.Label>Confirm Password:</Form.Label>
									<Form.Control type="password" placeholder="Confirm Password" required value={confirmPass} onChange={e => {setConfirmPass(e.target.value)}}/>
								</Form.Group>
								<div className="text-center">
									{
										isActive
										? <Button variant="primary" type="submit" className="mt-3 rounded-0 ">Register!</Button>
										: <Button variant="primary" type="submit" disabled className="mt-3 rounded-0 ">Register!</Button>
									}									
								</div>								
							</Form>
						</Card.Body>
					</Card>
				</Col>
			</Row>

		</>

	)

}