import {useState, useContext, useEffect} from 'react';
import {Form, Button, Card, Row, Col} from 'react-bootstrap';

import UserContext from '../userContext';

import {Navigate} from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Login() {

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false);

	useEffect(() => {

		if (email !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [email, password])

	function loginUser(e) {

		e.preventDefault();

		console.log(email);
		console.log(password);

		fetch('https://peaceful-anchorage-75608.herokuapp.com/users/login/', {

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {

			if(data.accessToken) {
				Swal.fire({
					icon: "success",
					title: "Login Successful",
					text: "Thank you for logging in."
				})

				localStorage.setItem('token', data.accessToken);

				let token = localStorage.getItem('token');


				fetch('https://peaceful-anchorage-75608.herokuapp.com/users/getUserDetails', {

					method: 'GET',
					headers: {
						'Authorization': `Bearer ${token}`
					}

				})
				.then(res => res.json())
				.then(data => {

					//localStorage.setItem('id', data._id);
					//localStorage.setItem('isAdmin', data.isAdmin);

					setUser({
						id: data._id,
						isAdmin: data.isAdmin
					})

				})

			} else {
				Swal.fire({
					icon: "error",
					title: "Login Failed",
					text: data.message
				})
			}

		})


	}

	return (

		user.id
		?
		<Navigate to="/products" replace={true} />
		:
		<>
			<h1 className="my-5 text-center" >Login</h1>
			<Row className="justify-content-center">
				<Col md={6} >
					<Card >
						<Card.Body>
							<Form onSubmit={e => loginUser(e)}>
								<Form.Group>
									<Form.Label>Email:</Form.Label>
									<Form.Control type="email" placeholder="" required value={email} onChange={e => {setEmail(e.target.value)}} />
								</Form.Group>
								<Form.Group className="pt-2">
									<Form.Label>Password:</Form.Label>
									<Form.Control type="password" placeholder="" required value={password} onChange={e => {setPassword(e.target.value)}} />
								</Form.Group>
								<div className="text-center">
									{
										isActive
										? <Button variant="primary" type="submit" className="mt-3 ">Login!</Button>
										: <Button variant="primary" disabled type="submit" className="mt-3 ">Login!</Button>
									}									
								</div>
							</Form>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</>

	)

}