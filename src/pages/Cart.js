import {useState, useEffect, useContext} from 'react';
import {Table, Button, Row, Col} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {Navigate} from 'react-router-dom';
import UserContext from '../userContext';
import {list, total, destroy, remove} from 'cart-localstorage';




export default function Cart() {

	const {user} = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);

	let cart = list();


	useEffect(() => {

		if (!user.isAdmin){


			setAllProducts(cart.map(product => {

				return (

					<tr key={product.id} className="text-center">
						<td><img className="p-1" height={35} src={product.url} /></td>
						<td className="text-start">{product.name}</td>
						<td>{product.price}z</td>
						<td>{product.quantity}</td>
						<td>{product.quantity * product.price}z</td>
						<td>
							<Button variant="danger" className="mx-2" onClick={() => {removeProduct(product.id)}} >Remove</Button>
						</td>
					</tr>

					)

			}))

		}

	}, [])


	console.log(allProducts.length);

	function checkout() {

		let products = cart.map(product => {
			return {productId: product.id,
					quantity: product.quantity}
		})

		console.log(total());
		console.log(products);


		fetch('https://peaceful-anchorage-75608.herokuapp.com/orders', {

			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				totalAmount: total(),
				products: products
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data.message)
			destroy();

			Swal.fire({
						icon: "success",
						title: "Successfully Checked Out!",
						timer: 2000
					})

			window.setTimeout(() => {window.location.href="/orders"}, 1500);

		})

	}

	function removeProduct(productId) {

		remove(productId);
		window.location.href="/cart";

	}


	return (

		user.isAdmin
		?
		<Navigate to="/products" replace={true} />
		:
		<>
			<h1 className="my-4 text-center" >My Cart</h1>
			<Row className="pb-3 text-end">
				<Col md={9}>
					<h4 >Grand Total: {total()}z</h4>
				</Col>
				<Col md={3} className="text-center">
					<Button variant="warning" onClick={checkout} disabled={allProducts.length === 0}>Checkout</Button>
				</Col>
			</Row>

			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Icon</th>
						<th>Name</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Total</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
						{allProducts}
				</tbody>
			</Table>
		</>
	)

}