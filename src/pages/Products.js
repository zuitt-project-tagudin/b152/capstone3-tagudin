import {useState, useEffect, useContext} from 'react';
import {Row, Col, Form, Button} from 'react-bootstrap';
import Banner from '../components/Banner';
import ProductCard from '../components/Product';
import AdminDashboard from '../components/AdminDashboard'
import UserContext from '../userContext';

export default function Products() {
	
	const {user} = useContext(UserContext);


	let productsBanner = {

		title: "Welcome to the Products Page",
		description: "View one of our products below.",
		buttonText: "Register/Login to Shop!",
		destination: "/register"

	}

	const [productsArray, setProductsArray] = useState([]);

	const [field, setField] = useState("");

	function searchProduct(e) {

		e.preventDefault();

		fetch('https://peaceful-anchorage-75608.herokuapp.com/products/findByName', {

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				name: field
			})

		})
		.then(res => res.json())
		.then(data => {


			setProductsArray(data.map(product => {

				return (
					
						<ProductCard key={product._id} productProp={product} />
					
					)
			}));


		})

	}

	useEffect(() => {

		fetch('https://peaceful-anchorage-75608.herokuapp.com/products/getActiveProducts')
		.then(res => res.json())
		.then(data => {


			setProductsArray(data.map(product => {

				return (
					
						<ProductCard key={product._id} productProp={product} />
					
					)
			}));


		})

	}, [])


/*
	let propCourse1 = "sample data 1";
	let propCourse2 = "sample data 2";
	let propCourse3 = "sample data 3";
*/

	
/*	let coursesComponents = coursesData.map(course => {

		return <CourseCard courseProp={course} key={course.id} />

	})
*/


	return (

		user.isAdmin
		? <AdminDashboard />
		:
		<>
			<Banner bannerProp={productsBanner} />
			<Row className="justify-content-center mb-3">
				<Col md={4}>
				<Form className="d-flex" onSubmit={searchProduct} >
				    <Form.Control type="text" placeholder="Search for an item" className=" mr-sm-2" value={field} onChange={e => {setField(e.target.value)}} />
				    <Button variant="secondary" className="mx-2" type="submit" >Search</Button>
				 </Form>
					
				</Col>
			</Row>
			<Row className="justify-content-center">
				{productsArray}				
			</Row>
		</>


		)


}